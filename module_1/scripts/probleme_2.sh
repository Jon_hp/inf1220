#!/bin/bash
for i in {0..100}
do
	marker=0
	echo "$i est inférieur à 100: On exécute le contenu du bloc TANT_QUE"
	echo "	Le programme affiche la valeur de l'itérateur: $i"
	if [[ $(echo "$i%3"|bc) == 0 ]]
	then
		echo "	$i est divisible par 3: le programme écrit \"Fizz\""
		marker=1;
	else
		echo "	$i n'est pas divisible par 3: On n'affiche pas \"Fizz\""
	fi
	if [[ $(echo "$i%5"|bc) == 0 ]]
	then
		if [[ $marker == 1 ]]
		then
			echo "	$i est aussi divisible par 5: le programme affiche \"Buzz\" aussi"
		else
			echo "	$i est divisible par 5: le programme affiche \"Buzz\""
		fi
	else
		echo "	$i n'est pas divisible par 5: On n'affiche pas \"Buzz\""
	fi
	echo "	On incrémente l'itérateur qui passe de $i à $(echo $i+1|bc)"
done
