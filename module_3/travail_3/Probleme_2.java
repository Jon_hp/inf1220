// La classe publique Problème_2 est accessible à l'extérieur du fichier,
// Représente "l'énoncé du problème".
// Elle contient les données des base
// Et elle appelle les méthodes/classes nécéssaires à la résolution
public class Probleme_2 {

	// L'énoncé demande des chaines de 4 caractères. 
	// On enregistre ce nombre dans une constante (final) de classe (static) visible uniquement par cette classe (private)
	// Dans un type "entier" 8 bits
	private static final byte LONGUEUR_DE_CHAINE = 4;

	// Les "caractères permis" de l'énoncé sont enregistrés dans un tableau de constantes globales accessibles par cette classe uniquement sans instanciation (static).
	private static final char[] CARACTERES_PERMIS = {'a','b','c','d'};

	// Méthode principale publique, donc accessible à toutes les autres classes, sans instanciation (static). Aucun retour (void):
	public static void main(String[] args) {

		// On crée une instance d'objet "Chaine"
		// En donnant les critères de l'énoncé en arguments au constructeur.
		Chaine chaine = new Chaine(LONGUEUR_DE_CHAINE,CARACTERES_PERMIS);
		// Celà donne accès aux méthodes d'instance qui permettent la résolution du problème.
		// On essaiera toutes les combinaisons de chaînes possibles (dans les limites données)
		// Et on imprimera celle qui répondent aux conditions
		// Les méthodes d'instances et leurs fonctionnements seront couverts en détails plus bas
		while(chaine.maxReached() == false){
			chaine.next();
			if(chaine.b_est_suivi_de_a() && chaine.ne_contient_pas_a_et_d()){
				chaine.nb_chaines_valides++;
				chaine.print();
			}
		}
		System.out.println(chaine.nb_chaines_valides);
	}
}
// Classe visible par les classes de ce fichier (sans modificateur d'accès)
// Classe qui sert à résoudre le problème.
class Chaine {

	// Variables "privées", donc visibles à l'intérieur de cette classe uniquement, avec instanciation (non statique)
	// Elle seront utilisées par les méthodes de l'instance.
	private short clock;
	private short possibilites;
	private char[] caracteresPermis;
	private char[] tableau;
	private String chaine;

	public	short nb_chaines_valides = 0;

	// Contructeur accessible aux autres classes.
	// Prend en entrée la longueur désirée des chaînes ainsi que les caractères (symboles) permis dans un tableau.
	// On met à 0 le compteur (clock) qui servira à générer toutes les combinaisons.
	// On crée une chaîne de caractères de longueur demandée.
	// On calcul le nombre de possibilités, compte tenu du nombre de symboles (caractères permis) et la longueur des chaînes demandée.
	// On enregistre les caractères permis (symboles) dans une variable d'instance (caracteresPermis).
	public Chaine(int longueur, char[] symboles){
		this.clock = 0;
		this.tableau = new char[longueur];
		this.possibilites = (short) Math.pow(symboles.length, this.tableau.length);
		this.caracteresPermis = symboles;
	}
	// Méthode publique (visible par les autres classes)
	// Sert à tester une combinaison de "chaine" pour la condition éponyme.
	// Retournera soit vrai, soit faux (boolean)
	public boolean b_est_suivi_de_a(){

		// On utilise une variable locale nommée "itérateur" pour passer en revue chaque caractère de la chaîne.
		int iterateur = 0;
		// Tant que l'itérateur est inférieur à la longueur de la chaîne...
		while (iterateur < this.tableau.length){
			// On vérifie si le caractère est 'b'.
			if (this.tableau[iterateur] == 'b'){
				// Pour éviter les erreurs d'indexation et éviter de tester un élément inexistant,
				// On retourne faux et on sort de la boucle si la dernière lettre de la chaîne est 'b'.
				if (iterateur == (this.tableau.length - 1)) return false;
				// Dans tous les autres cas, si le caractère suivant n'est pas un 'a', la méthode renvoie "faux"
				else if(this.tableau[iterateur + 1] != 'a') return false;
			}
			// On incrémente l'itérateur pour accéder au prochain élément (ou sortir de la boucle)
			iterateur++;
		}
		// Si aucune condition d'exclusion n'a été déclarée, la méthode renvoie "vrai" 
		// Et confirme la condition éponyme
		return true;
	}
	// Méthode publique (visible par les autres classes)
	// Sert à tester une chaine de la condition éponyme 
	// Retournera soit vrai, soit faux (boolean)
	public boolean ne_contient_pas_a_et_d(){

		// Variables locales:
		// Un entier itérateur (8 bits) afin d'accéder aux éléments du tableau:
		byte iterateur = 0;
		// Et deux boolean de 1 bit qui serviront à vérifier si des conditions sont rencontrées:
		boolean contient_a = false;
		boolean contient_d = false;

		// Tant que l'itérateur est inférieur à la longueur de la chaîne...
		while (iterateur < this.tableau.length) {

			// On vérifie le caractère à la position de l'itérateur dans le tableau avec une structure de contrôle "switch"
			switch(this.tableau[iterateur]) {

				// Dans le cas où le caractère trouvé à la position de l'itérateur soit 'd', on lève le "drapeau" (boolean) correspondant.
				// Et on sort de la structure switch (break)
				case 'd':
					contient_d = true;
					break;
				// Même principe.
				case 'a':
					contient_a = true;
					break;
			}

			// À tout moment, si les deux boolean sont vrais, on quitte la méthode en retournant faux
			if (contient_a && contient_d) return false;
			iterateur++;
		}
		// Ayant évalué toute la chaîne sans rencontrer la condition d'exclusion, on renvoit vrai
		return true;
	}
	// Méthode accessible aux autres classes (public) via l'instance (non-static)
	// Transforme la chaîne en sa prochaine combinaison possible selon l'ordre lexicographique.
	public void next(){

		// On crée une variable locale "itérateur"
		// Qui permettra d'accéder aux éléments de la chaine (entier 8 bits: valeur maximale = 255)
		byte iterateur = 0;
		// Un entier "diviseur" qui recevra une valeur servant à diviser le compteur (clock) selon un poids inversement proportionnel à la position de l'élément dans la "chaîne" (valeur maximale = 255)
		byte diviseur;
		// Un entier "exposant" de 8 bits qui enregistrera temporairement l'inverse de l'indice du caractère en cours (itérateur)
		byte exposant;
		// Entier de 16 bits qui recevra un nouveau compteur divisé par une puissance de 4
		short divided_clock;
		byte base = (byte)this.caracteresPermis.length;
		byte modulo;
		// Tant que l'itérateur est inférieur à la longueur de la chaîne,
		while (iterateur < this.tableau.length) {
			// Le diviseur sert à diviser la "fréquence" du compteur selon un "poids" déterminé par une puissance de la base (nombres de symboles possibles) en l'occurence 4, élevée à l'exposant (inverse de la position dans le tableau >> Lexicographique)
			exposant = (byte)((this.tableau.length - 1) - iterateur);
			diviseur = (byte)Math.pow(base, exposant);
			// On obtient un compteur réduit, inversement proportionnel au poids de la position (itérateur)
			divided_clock = (short)(clock / diviseur);
			// Le reste de la division euclidienne de ce nouveau compteur par le nombre de caractères permis (base) permet de choisir un caractère parmis ceux du tableau de "caractères permis"
			modulo = (byte)(divided_clock % base);
			// On attribut le caractère choisi à la variable du tableau "chaine" à la position de l'itérateur
			this.tableau[iterateur] = this.caracteresPermis[modulo];
			// et on incrémente l'itérateur
			iterateur++;
		}
		// Et le "compteur"
		this.clock++;
	}
	// Méthode accessible aux autres classe (public) par l'instance (non-static)
	// Concatène les caractères et affiche la chaîne
	public void print(){

		// On "vide" le contenu de la "String"
		this.chaine = "";
		// Et on concatène les caractères choisis par l'algorithme dans notre chaîne.
		for (int i = 0;i < this.tableau.length; i++){
			this.chaine += Character.toString(this.tableau[i]); 
		}
		System.out.println(this.chaine);

	}
	// Méthode accessible aux autres classe (public)
	// Sert à vérifier si le compteur (clock) a atteint le maximum
	// C'est-à-dire si toutes les combinaisons ont été essayées
	// Type "boolean" donc retournera "vrai" ou "faux"
	public boolean maxReached(){

		if(this.clock < this.possibilites){
			return false;
		}
		return true;
	}
}