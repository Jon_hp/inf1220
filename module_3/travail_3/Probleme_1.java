// La classe Problème_1 sert à solutionner le problème
// C’est la seule classe publique du fichier du même nom.
// Est accessible par toutes les autres classes (ou fichier)
public class Probleme_1 {
  // La classe ne contient que la méthode “main”
  // Cette méthode est publique. Elle est donc visible par les autres classes (externes)
  // Est accessible sans instanciation (statique)
  // La méthode ne retourne rien (void)
  // L'argument de la méthode est un tableau de chaînes de charactères (strings)
  public static void main(String[] args){

    // Variables locales de types entiers 32 bits:
    int centaines;
    int nombre;
    int somme = 0;

    // Boucle qui énumère les nombres de 1 à 10 000 (inclusivement)
    for(nombre = 1; nombre <= 10000; nombre++){

      // Le quotient de la division euclidienne du nombre par cent nous donne le nombre de centaines. Le reste de la division par dix du nombre de centaines correspond aux "unités" de centaines, donc le chiffre à la position des centaines du nombre.
      centaines = (nombre/100) % 10;

      // Trois conditions doivent être réunies pour que le nombre soit additionné:
      // 1. Le nombre n'est pas divisible par 3 (il y a un reste)
      // 2. Le chiffre à la position des centaines n'est pas 2 
      // 3. Le chiffre à la position des centaines n'est pas 3  
      if(nombre % 3 != 0 && centaines != 2 && centaines != 3){
        // Si les trois conditions sont rencontrées (&&), on additionne le nombre.
        somme += nombre;
      }
    }
    // Une fois sorti de la boucle, on affiche le résultat (somme).
    System.out.print(somme);
  }
}