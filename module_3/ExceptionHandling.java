// Librairie des flux (stream) entrées/sorties
import java.io.*;
// Classe principale:
public class ExceptionHandling {

	public static void main (String[] args) {
		try {
			FileReader reader = new FileReader("partie1.txt");
			System.out.println("file is opened");
		} catch(FileNotFoundException ex) {
			System.out.println("file was not found.");
		}
	}
}
