// Question #2

// Un programmeur souhaite représenter la valeur 10.000000000000001 en Java. 
// Il a écrit le programme suivant. Exécutez le programme et expliquez le résultat.
class Main {

	public static void main(String[] args) {

		double x =  10.000000000000001;
		System.out.println(x);

	}
}
// En java, le type double a une taille de 64 bits(8 octets): Le premier bit représente le signe (0=positif, 1=négatif). Les onzes bits suivant contiennent l'exposant. Et les 52 autres bits contienneint la valeur de la mantisse.
// Ici, la mantisse (10000000000000001 en base 10) correspond à 100011100001101111001001101111110000010000000000000000 en base 2, pour un total de 55 bits, ce qui dépasse la capacité du type double. Ainsi on assiste à un arrondissement binaire et la valeur stockée et afficher par le programme est 10.000000000000002.
