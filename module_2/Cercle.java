// Question #1
// Classe principale portant le même nom que le fichier JAVA
// Mot-clé "public" pour être accessible de l'extérieur (classe/programme)
public class Cercle {

	// Variable d'instance d'objet Cercle (non-static)
	// Accessible via une méthode de l'instance
	// Type: Virgule flottante haute précision (64 bits)
	private double rayon;

	// Constructeur
	// Méthode effectuée au moment de l'instanciation
	// Attribution de la valeur donnée en argument à la variable "rayon".
	// Maximum 16 chiffres après la virgule.
	Cercle(double rayon) {
		this.rayon = rayon;
	}
	// Méthode qui calcule l'aire
	// Accessible uniquement par la classe "Cercle"(private)
	private double aire() {

		return Math.PI * Math.pow(rayon,2);

	}
	// Méthode qui calcule le perimetre
	// Accessible uniquement par la classe "Cercle" (private)
	private double perimetre() {

		return 2 * Math.PI * rayon;

	}
	public static void main(String[] args) {

		Main.main();

	}
}
// Si la valeur de rayon donnée par l'argument est zéro, le programme fonctionnera normalement et les méthodes aire() et perimetre() retouneront des valeurs nulles (0). Si une valeur négative était donnée au rayon, la méthode aire() donnerait une valeur positive (carré d'un nombre négatif) et la méthode perimetre() retournerait une valeur négative.

// Question #2

// Un programmeur souhaite représenter la valeur 10.000000000000001 en Java. 
// Il a écrit le programme suivant. Exécutez le programme et expliquez le résultat.
class Main {

	public static void main(String[] args) {

		double x =  10.000000000000001;
		System.out.println(x);

	}
}
// En java, le type double a une taille de 64 bits(8 octets): Le premier bit représente le signe (0=positif, 1=négatif). Les onzes bits suivant contiennent l'exposant. Et les 52 autres bits contiennent la valeur de la mantisse.
// Ici, la mantisse (10000000000000001 en base 10) correspond à 100011100001101111001001101111110000010000000000000000 en base 2, pour un total de 55 bits, ce qui dépasse la capacité du type double. Ainsi on assiste à un arrondissement binaire et la valeur stockée en mémoire et afficher par le programme est 10.000000000000002.

// Question #3

Quelle sera la valeur de ‘a’ après les lignes de code suivantes et pourquoi :

// On déclare un entier 32 bit et on attribue la valeur 3;
int i = 3;
// On déclare un autre entier, auquel on attribue la valeur de i, c'est-à-dire 3, qu'on incrémente immédiatement après l'attribution (post-incrémentation)
int a = i++;
// a = 3
// i = 4

// Question 4:

La méthode main déclare une chaîne de caractères et y enregistre le mot "test". On appelle ensuite la méthode (appelée "test") avec comme argument la variable "chaîne". La méthode "test" concatène la valeur de l'argument avec elle-même. (test + test). Le résultat affiché sera donc "testtest".

// Question 5:

Quelle sera la valeur de la variable entier à la fin du code suivant et pourquoi ?

boolean a = false;
boolean b = false;
        	
int entier = (!a && (b | !a)) ? 10 : 20;

// En suivant la priorité des opérations indiquée par les parenthèse, on obtient:

(b | !a) = (false|!false) = (false|true) = true
!a && (true) = !false && true == true && true = true

Selon la nomenclature de l'opérateur à trois opérande, la valeur attribuée à l'entier "entier" sera de 10 puisque l'énoncée est vraie.

// Question 6

Les deux exemples représentants une classe "Bonhomme" sont identiques, à l'exception de la variable "nom", qui est déclarée comme variable de classe (static) dans le premier exemple, et comme variable d'instance (non static) dans le deuxième.
Celà signifie que dans le premier exemple la variable nom sera accessible directement via la classe "Bonhomme", alors que dans le second cas la classe "Bonhomme" doit être instanciée par le constructeur pour avoir accès à la variable "nom", tel que:

Bonhomme John = new Bonhomme("John");
System.out.println(John.nom);
