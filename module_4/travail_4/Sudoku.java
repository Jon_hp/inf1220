// Importation du paquet des flux d'entrée et sorties
import java.io.*;
// Et du paquet d'interface du système de fichiers
import java.nio.file.*;

// Déclaration d'exception personnalisée:
// Cette classe est visible seulement aux classes dans ce fichier
class SudokuInvalideException extends Exception {
  SudokuInvalideException(String msg) {
    super("Sudoku Invalide: " + msg);
  }
}
// Classe principale
// Est visible par l'extérieur de ce fichier
public class Sudoku {

  // Variable globales d'instance:
  // Visibles à l'intérieur de la classe Sudoku (privées)
  private String sourceFileName = "partie1.txt";
  private byte[][] matrix = new byte[9][9];
  private byte[] fileContent;

  // Méthode visible dans la classe Sudoku
  // Sans retour: La méthode agit sur des variables d'instance globales
  // Sert à charger le contenu du fichier-sudoku dans la string "fileContent"
  private void loadFile() throws java.nio.file.NoSuchFileException, IOException {

    this.fileContent = Files.readAllBytes(Paths.get(this.sourceFileName));

  } 
  // Visible à l'intérieur de la classe Sudoku (privée)
  // Sans retour: La méthode agit sur des variables d'instance globales
  // Méthode qui convertie une string de triplets séparés par espaces en tableau d'entiers(2D)
  // Si le fichier (de triplets) contient autre chose que des triplets,
  // Ou si le fichier contient plus de triplets que le nombre prévu,
  // La méthode pourra générer (throw) des exceptions qu'elle transferera à (main)(ou la méthode qui l'appelle)
  private void toMatrix() throws NullPointerException, ArrayIndexOutOfBoundsException {

    // On déclare un tableau de trois entiers 
    // qui servira à enregistrer temporairement les caractères d'un triplet.
    byte[] triplet = new byte[3];
    // Et d'un index pour accéder à ces données temporaires
    int temp_index = 0;

    for(int i = 0;i < this.fileContent.length; i++) {

      // On passe en revue chaque caractère du tableau "fileContent"
      switch (this.fileContent[i]) {
        // On utilise les espaces (ascii 32) et la nouvelle ligne (ascii 10) (à la fin du fichier) 
        case 32:
        case 10:
          // Pour enregistrer la valeur (z) du triplet aux coordonnés (x,y) de la matrice
          this.matrix[triplet[0]][triplet[1]] = triplet[2];
          // Et remettre à zéro le triplet temporaire
          temp_index = 0;
          break;
        default:
          // Autrement dit, si le caractère n'est ni un espace, ni une nouvelle ligne,
          // On enregistre le caractère dans notre "triplet temporaire" 
          // Et on incrémente l'"index temporaire"
          triplet[temp_index++] = (byte)(this.fileContent[i] - 48);
      }
    }
  }
  private void validate() throws SudokuInvalideException {

    byte nFound;
    for(byte digit = 1; digit <= 9; digit++) {

      // vérification par colonnes.
      for (int x=0;x < 9; x++) {
        nFound = 0;
        for(int y = 0;y < 9; y++) {
          if(this.matrix[x][y] == digit) {
		    nFound++;
		  }
		}
        if (nFound == 0) 
          throw new SudokuInvalideException("La colonne " + x + " ne contient pas le chiffre " + digit);  
        else if (nFound > 1) 
          throw new SudokuInvalideException("La colonne " + x + " contient le chiffre " + digit + " " + nFound + " fois.");  
      }
      // vérification par rangee.
      for (int y=0;y < 9; y++) {
        nFound = 0;
        for (int x=0;x < 9; x++) 
          if(this.matrix[x][y] == digit) nFound++;
        if (nFound == 0) 
          throw new SudokuInvalideException("La rangée " + y + " ne contient pas le chiffre " + digit);  
        else if (nFound > 1) 
          throw new SudokuInvalideException("La rangée " + y + " contient le chiffre " + digit + " " + nFound + " fois.");  
      }
    }
    System.out.println("Le sudoku est valide!");
  }
  // Visible à l'intérieur de la classe Sudoku (privée)
  // Sans retour: La méthode agit sur des variables d'instance globales
  // Cette méthode affiche le sudoku en format sudoku à l'écran
  private void render() {

    for (int y=0;y < 9; y++) {
      for (int x=0;x < 9; x++) {
        System.out.print(this.matrix[x][y]);
        System.out.print("   ");
      }  
      System.out.print("\n\n");
    }
  }
  // Méthode visible dans la classe Sudoku
  // Convertie le tableau (2D) "matrix" en une string de triplets (séparés par espaces)
  private String format() {

    String tripletsString = new String();

    for (int y=0;y < 9; y++) 
      for (int x=0;x < 9; x++){
        char[] triplet = {(char)(x+48),(char)(y+48),(char)(this.matrix[x][y]+48)};
        tripletsString += new String(triplet);
        if(x!=8 || y!=8)
          tripletsString += ' ';
      }
      tripletsString += "\n";

    return tripletsString;
  }

  // Visible à l'intérieur de la classe Sudoku (privée)
  // Sans retour: La méthode agit sur des variables d'instance globales
  // Méthode servant à écrire la matrice formatée en triplets dans un nouveau fichier
  // Risque de générer (throw) une erreur d'entrées/sortie.
  // On transmet le message d'erreur (objet) à la méthode qui appelle celle-ci  
  private void writeToFile(String newFileName) throws IOException
  {
    PrintWriter writer = new PrintWriter(newFileName, "UTF-8");
    writer.print(this.format());
    writer.close();
  }
  // Visible à l'intérieur de la classe Sudoku (privée)
  // Sans retour: La méthode modifie les données (globales) de la matrice 
  // À l'aide d'une copie (clone) du tableau
  private void transpose() {

    byte[][] copy = this.matrix.clone();
    for (int y=0;y<9;y++){
      copy[y] = this.matrix[y].clone();
    }
    for (int y=0;y<9;y++){
      for (int x=0;x<9;x++){ 
        this.matrix[x][y] = copy[y][x];
      }
    }
  }
  // Méthode principale
  // Accessible sans instanciation (statique)
  // Visible à l'extérieur de la classe Sudoku (publique)
  public static void main(String[] args) {

    // Instanciation de la classe Sudoku
    Sudoku sudoku = new Sudoku();

    // Si un nom de fichier est donné comme argument, l'utiliser pour charger le sudoku. Sinon on utilisera "partie1.txt" tel qu'initialisé.
    if (args.length != 0) sudoku.sourceFileName = args[0];

    // Bloc try-catch de lecture du fichier-sudoku
    try  
    { 
      sudoku.loadFile();
      // Si la lecture dans le fichier a réussi, on convertit le contenu du fichier en matrice (tableau à deux dimensions)
      // En gérant les exceptions potentielles.
      try  
      { 
        sudoku.toMatrix();
      }
      catch (NullPointerException ex)
      {
        System.out.println("Erreur: Le fichier est vide."); 
      }
      catch (ArrayIndexOutOfBoundsException ex)
      {
        System.out.println("Erreur: Les coordonnées (x,y) doivent être des chiffres entre 0 et 8 inclusivement"); 
      }
      // La méthode de validation du sudoku lancera une exception de type “SudokuInvalidException” si le sudoku est invalide
      try {
        sudoku.validate();
      }
      catch (SudokuInvalideException ex) {
        System.out.println(ex.getMessage());
      }
      // On affichera ensuite le sudoku (en format sudoku) 
      System.out.println("Sudoku:");
      sudoku.render();
      // Et on effectue la transposition.
      sudoku.transpose();
      // qu’on affichera aussi à l’écran (format sudoku)
      System.out.println("Transposé:");
      sudoku.render();
      // Écriture du sudoku transposé dans un nouveau fichier
      try {
        String newFileName = sudoku.sourceFileName.replaceAll(".txt","_transposed.txt");
        sudoku.writeToFile(newFileName);
        System.out.println("Le sudoku transposé s'est correctement enregistré dans le fichier " + newFileName);
      }
      // Gestion des erreurs d'écriture dans le fichier
      catch (IOException ex) 
      {  
        System.out.println(ex.getMessage()); 
      }
    }
    // Si le fichier n’a pas pu être chargé et une exception est émise,
    // On arrive directement ici et un message d’erreur correspondant est affiché
    catch (java.nio.file.NoSuchFileException ex) 
    {  
      System.out.println("Fichier " + sudoku.sourceFileName + " introuvable");
    }
    catch (IOException ex) 
    {  
      System.out.println(ex.getMessage()); 
    }
  }
}
